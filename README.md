**Compatibility Lib**

This lib is designed for minecraft shaders running optifine and serves to allow developers who target compatibility with all platforms have a full feature set. this lib will be constantly evolving and the checklist is as follows  

Windows shader5 support should be complete for all hardware (AMD may not work)  
Mac is in progress shader4 has extreme cutbacks  mainly it is bitwise functions that do not work  
Linux is in a similar boat to mac, this is constantly being developed so check back often.  

Functions that have //verify or //verified are proven to be correct compaired to the driver.  
Matrix math still needs testing as do bitwise ops