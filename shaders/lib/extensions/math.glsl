#include "/lib/extensions/math/trig.glsl"
    #include "/lib/extensions/math/integer.glsl"
#ifdef GPU_SHADER4
    //FMA function
    #define fma(a,b,c) (a * b + c)
    #define isnan(x) (x != x)
    #define isinf(x) (x != 0.0 && x * 2.0 == x)

    //RoundEven function. 
    float roundEven(float x) { float y = round(x); return (y - x == 0.5) ? floor(0.5 * y) * 2.0 : y; }
    vec2  roundEven(vec2 x)  { vec2 y = round(x);  return (y - x == vec2(0.5)) ? floor(0.5 * y) * 2.0 : y; }
    vec3  roundEven(vec3 x)  { vec3 y = round(x);  return (y - x == vec3(0.5)) ? floor(0.5 * y) * 2.0 : y; }
    vec4  roundEven(vec4 x)  { vec4 y = round(x);  return (y - x == vec4(0.5)) ? floor(0.5 * y) * 2.0 : y; }

    //Frexp
    float frexp(float x, out int e  ) { e = int(  ceil(log2(x))); return(x * exp2(-e)); } //Semi wrong, prefers large numbers
    vec2  frexp(vec2 x,  out ivec2 e) { e = ivec2(ceil(log2(x))); return(x * exp2(-e)); }
    vec3  frexp(vec3 x,  out ivec3 e) { e = ivec3(ceil(log2(x))); return(x * exp2(-e)); }
    vec4  frexp(vec4 x,  out ivec4 e) { e = ivec4(ceil(log2(x))); return(x * exp2(-e)); }

    //ldexp
    float ldexp(float x, int e)  { return x * pow(2.0, e); } //valid
    vec2  ldexp(vec2 x, ivec2 e) { return x * pow(vec2(2.0), e); }
    vec3  ldexp(vec3 x, ivec3 e) { return x * pow(vec3(2.0), e); }
    vec4  ldexp(vec4 x, ivec4 e) { return x * pow(vec4(2.0), e); }

    #include "/lib/extensions/math/matrix.glsl"

#endif
