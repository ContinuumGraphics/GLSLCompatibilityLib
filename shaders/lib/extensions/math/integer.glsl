//Reversal Functions
int bitfieldReverse(int x) {
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1);
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2);
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4);
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8);
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16);
    return x;
}

ivec2 bitfieldReverse(ivec2 x) {
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1);
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2);
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4);
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8);
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16);
    return x;
}

ivec3 bitfieldReverse(ivec3 x) {
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1);
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2);
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4);
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8);
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16);
    return x;
}

ivec4 bitfieldReverse(ivec4 x) {
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1);
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2);
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4);
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8);
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16);
    return x;
}

uint bitfieldReverse(uint x) {
    x = ((x & 0x55555555u) << 1u) | ((x & 0xAAAAAAAAu) >> 1u);
    x = ((x & 0x33333333u) << 2u) | ((x & 0xCCCCCCCCu) >> 2u);
    x = ((x & 0x0F0F0F0Fu) << 4u) | ((x & 0xF0F0F0F0u) >> 4u);
    x = ((x & 0x00FF00FFu) << 8u) | ((x & 0xFF00FF00u) >> 8u);
    x = ((x & 0x0000FFFFu) << 16u) | ((x & 0xFFFF0000u) >> 16u);
    return x;
}

uvec2 bitfieldReverse(uvec2 x) {
    x = ((x & 0x55555555u) << 1u) | ((x & 0xAAAAAAAAu) >> 1u);
    x = ((x & 0x33333333u) << 2u) | ((x & 0xCCCCCCCCu) >> 2u);
    x = ((x & 0x0F0F0F0Fu) << 4u) | ((x & 0xF0F0F0F0u) >> 4u);
    x = ((x & 0x00FF00FFu) << 8u) | ((x & 0xFF00FF00u) >> 8u);
    x = ((x & 0x0000FFFFu) << 16u) | ((x & 0xFFFF0000u) >> 16u);
    return x;
}

uvec3 bitfieldReverse(uvec3 x) {
    x = ((x & 0x55555555u) << 1u) | ((x & 0xAAAAAAAAu) >> 1u);
    x = ((x & 0x33333333u) << 2u) | ((x & 0xCCCCCCCCu) >> 2u);
    x = ((x & 0x0F0F0F0Fu) << 4u) | ((x & 0xF0F0F0F0u) >> 4u);
    x = ((x & 0x00FF00FFu) << 8u) | ((x & 0xFF00FF00u) >> 8u);
    x = ((x & 0x0000FFFFu) << 16u) | ((x & 0xFFFF0000u) >> 16u);
    return x;
}

uvec4 bitfieldReverse(uvec4 x) {
    x = ((x & 0x55555555u) << 1u) | ((x & 0xAAAAAAAAu) >> 1u);
    x = ((x & 0x33333333u) << 2u) | ((x & 0xCCCCCCCCu) >> 2u);
    x = ((x & 0x0F0F0F0Fu) << 4u) | ((x & 0xF0F0F0F0u) >> 4u);
    x = ((x & 0x00FF00FFu) << 8u) | ((x & 0xFF00FF00u) >> 8u);
    x = ((x & 0x0000FFFFu) << 16u) | ((x & 0xFFFF0000u) >> 16u);
    return x;
}

//LSB and MSB Functions
int   findLSB(int x  ) { return x&-x; }
ivec2 findLSB(ivec2 x) { return x&-x; }
ivec3 findLSB(ivec3 x) { return x&-x; }
ivec4 findLSB(ivec4 x) { return x&-x; }

uint  findLSB(uint x ) { return x&-x; }
uvec2 findLSB(uvec2 x) { return x&-x; }
uvec3 findLSB(uvec3 x) { return x&-x; }
uvec4 findLSB(uvec4 x) { return x&-x; }

int findMSB(int x) {
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);
    return (x & ~(x >> 1));
}

ivec2 findMSB(ivec2 x) {
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);
    return (x & ~(x >> 1));
}

ivec3 findMSB(ivec3 x) {
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);
    return (x & ~(x >> 1));
}

ivec4 findMSB(ivec4 x) {
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);
    return (x & ~(x >> 1));
}



uint findMSB(uint x) {
    x |= (x >> 1u);
    x |= (x >> 2u);
    x |= (x >> 4u);
    x |= (x >> 8u);
    x |= (x >> 16u);
    return (x & ~(x >> 1u));
}

uvec2 findMSB(uvec2 x) {
    x |= (x >> 1u);
    x |= (x >> 2u);
    x |= (x >> 4u);
    x |= (x >> 8u);
    x |= (x >> 16u);
    return (x & ~(x >> 1u));
}

uvec3 findMSB(uvec3 x) {
    x |= (x >> 1u);
    x |= (x >> 2u);
    x |= (x >> 4u);
    x |= (x >> 8u);
    x |= (x >> 16u);
    return (x & ~(x >> 1u));
}

uvec4 findMSB(uvec4 x) {
    x |= (x >> 1u);
    x |= (x >> 2u);
    x |= (x >> 4u);
    x |= (x >> 8u);
    x |= (x >> 16u);
    return (x & ~(x >> 1u));
}


//Bit count Functions
int bitCount(int a) {
    a = (a & 0x55555555) + ((a >>  1) & 0x55555555); 
    a = (a & 0x33333333) + ((a >>  2) & 0x33333333); 
    a = (a + (a >> 4)) & 0x0f0f0f0f; 
    a = (a + (a >> 8)); 
    a = (a + (a >> 16)); 
    return a & 0xff;
}

ivec2 bitCount(ivec2 a) {
    a = (a & 0x55555555) + ((a >>  1) & 0x55555555); 
    a = (a & 0x33333333) + ((a >>  2) & 0x33333333); 
    a = (a + (a >> 4)) & 0x0f0f0f0f; 
    a = (a + (a >> 8)); 
    a = (a + (a >> 16)); 
    return a & 0xff;
}

ivec3 bitCount(ivec3 a) {
    a = (a & 0x55555555) + ((a >>  1) & 0x55555555); 
    a = (a & 0x33333333) + ((a >>  2) & 0x33333333); 
    a = (a + (a >> 4)) & 0x0f0f0f0f; 
    a = (a + (a >> 8)); 
    a = (a + (a >> 16)); 
    return a & 0xff;
}

ivec4 bitCount(ivec4 a) {
    a = (a & 0x55555555) + ((a >>  1) & 0x55555555); 
    a = (a & 0x33333333) + ((a >>  2) & 0x33333333); 
    a = (a + (a >> 4)) & 0x0f0f0f0f; 
    a = (a + (a >> 8)); 
    a = (a + (a >> 16)); 
    return a & 0xff;
}


uint bitCount(uint a) {
    a = (a & 0x55555555u) + ((a >>  1u) & 0x55555555u); 
    a = (a & 0x33333333u) + ((a >>  2u) & 0x33333333u); 
    a = (a + (a >> 4u)) & 0x0f0f0f0fu; 
    a = (a + (a >> 8u)); 
    a = (a + (a >> 16u)); 
    return a & 0xffu;
}

uvec2 bitCount(uvec2 a) {
    a = (a & 0x55555555u) + ((a >>  1u) & 0x55555555u); 
    a = (a & 0x33333333u) + ((a >>  2u) & 0x33333333u); 
    a = (a + (a >> 4u)) & 0x0f0f0f0fu; 
    a = (a + (a >> 8u)); 
    a = (a + (a >> 16u)); 
    return a & 0xffu;
}

uvec3 bitCount(uvec3 a) {
    a = (a & 0x55555555u) + ((a >>  1u) & 0x55555555u); 
    a = (a & 0x33333333u) + ((a >>  2u) & 0x33333333u); 
    a = (a + (a >> 4u)) & 0x0f0f0f0fu; 
    a = (a + (a >> 8u)); 
    a = (a + (a >> 16u)); 
    return a & 0xffu;
}

uvec4 bitCount(uvec4 a) {
    a = (a & 0x55555555u) + ((a >>  1u) & 0x55555555u); 
    a = (a & 0x33333333u) + ((a >>  2u) & 0x33333333u); 
    a = (a + (a >> 4u)) & 0x0f0f0f0fu; 
    a = (a + (a >> 8u)); 
    a = (a + (a >> 16u)); 
    return a & 0xffu;
}