#define sinh(x) ((exp(x) - exp(-x)) * 0.5) //valid
#define cosh(x) ((exp(x) + exp(-x)) * 0.5) //valid
#define tanh(x) ((exp(2.0 * x) - 1.0) / (exp(2.0 * x) + 1.0)) //wrong 0.5 the time. which is intersting because it seems to be perfect as an algorithm

#define asinh(x) (log(x + sqrt(x * x + 1.0))) //valid
#define acosh(x) (log(x + sqrt(x * x - 1.0))) //valid
#define atanh(x) (log((1.0 + x) / (1.0 - x)) / 2.0) //wrong

#define trunc(x) (truncate(x)) //valid
