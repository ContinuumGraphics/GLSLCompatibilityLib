//This file will be split up into many sections for each vendor with multiple GL versions.

#if defined(OS_WINDOWS)
    #if GL_VERSION >= 330 && GLSL_VERSION >= 150
        #extension GL_ARB_gpu_shader5 : enable
        #extension GL_ARB_gpu_shader_fp64 : enable
        #define GPU_SHADER5

        #if defined(GL_VENDOR_INTEL)
            #define unsignedFix
        #endif
    #elif GL_VERSION < 330 && GL_VERSION <= 200
        #extension GL_EXT_gpu_shader4 : enable
        #define doubleRemap
        #define GPU_SHADER4

        #if defined(GL_VENDOR_INTEL)
            #define unsignedFix
        #endif
    #endif
#elif defined(OS_MAC)
    //#if GL_VERSION >= 330 //Mac doesnt support shader 5 for mac because it requires glsl 150 which is not the contex defined in minecraft
        #extension GL_EXT_gpu_shader4 : enable
        #define doubleRemap
        #define GPU_SHADER4

        #if defined(GL_VENDOR_INTEL)
            #define unsignedFix
        #endif
    //#endif
#else //More OS's to come

#endif

#if defined(unsignedFix)
    #define uint unsigned int
#endif

#if defined(doubleRemap)
    #define double float
    #define dvec2 vec2
    #define dvec3 vec3
    #define dvec4 vec4

    #define dmat2 mat2
    #define dmat3 mat3
    #define dmat4 mat4

    #define dmat2x2 mat2x2
    #define dmat2x3 mat2x3
    #define dmat2x4 mat2x4

    #define dmat3x2 mat3x2
    #define dmat3x3 mat3x3
    #define dmat3x4 mat3x4

    #define dmat4x2 mat4x2
    #define dmat4x3 mat4x3
    #define dmat4x4 mat4x4
#endif

#include "/lib/extensions/math.glsl"