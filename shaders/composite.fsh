#version 120
#include "/lib/extensions.glsl"

uniform sampler2D colortex0;
uniform sampler2D colortex1;

uniform sampler2D gdepthtex;
uniform mat4 gbufferProjectionInverse;

varying vec2 texcoord;

/* DRAWBUFFERS:0 */ 

void functionTest() {
    float float1 = 0.0;
    const float cfloat1 = 0.0;

    double double1 = 0.0;
    const double cdouble1 = 0.0;

    int int1 = 0;
    const int cint1 = 0;

    uint uint1 = 0u;
    const uint cuint1 = 0u;

    bool bool1 = true;
    const bool cbool1 = true;


    vec2 vec21 = vec2(0.0);
    const vec2 cvec21 = vec2(0.0);

    ivec2 ivec21 = ivec2(0);
    const ivec2 civec21 = ivec2(0);

    uvec2 uvec21 = uvec2(0u);
    const uvec2 cuvec21 = uvec2(0u);

    dvec2 dvec21 = dvec2(0.0);
    const dvec2 cdvec21 = dvec2(0.0);

    bvec2 bvec21 = bvec2(true);
    const bvec2 cbvec21 = bvec2(true);

    
    vec3 vec31 = vec3(0.0);
    const vec3 cvec31 = vec3(0.0);

    ivec3 ivec31 = ivec3(0);
    const ivec3 civec31 = ivec3(0);

    uvec3 uvec31 = uvec3(0u);
    const uvec3 cuvec31 = uvec3(0u);

    dvec3 dvec31 = dvec3(0.0);
    const dvec3 cdvec31 = dvec3(0.0);

    bvec3 bvec31 = bvec3(true);
    const bvec3 cbvec31 = bvec3(true);


    vec4 vec41 = vec4(0.0);
    const vec4 cvec41 = vec4(0.0);

    ivec4 ivec41 = ivec4(0);
    const ivec4 civec41 = ivec4(0);

    uvec4 uvec41 = uvec4(0u);
    const uvec4 cuvec41 = uvec4(0u);

    dvec4 dvec41 = dvec4(0.0);
    const dvec4 cdvec41 = dvec4(0.0);

    bvec4 bvec41 = bvec4(true);
    const bvec4 cbvec41 = bvec4(true);


    mat2 mat21 = mat2(0.0);
    const mat2 cmat21 = mat2(0.0);

    mat3 mat31 = mat3(0.0);
    const mat3 cmat31 = mat3(0.0);

    mat4 mat41 = mat4(0.0);
    const mat4 cmat41 = mat4(0.0);


    dmat2 dmat21 = dmat2(0.0);
    const dmat2 cdmat21 = dmat2(0.0);

    dmat3 dmat31 = dmat3(0.0);
    const dmat3 cdmat31 = dmat3(0.0);

    dmat4 dmat41 = dmat4(0.0);
    const dmat4 cdmat41 = dmat4(0.0);

    mat2x2 mat2x21 = mat2x2(0.0);
    const mat2x2 cmat2x21 = mat2x2(0.0);
    mat2x3 mat2x31 = mat2x3(0.0);
    const mat2x3 cmat2x31 = mat2x3(0.0);
    mat2x4 mat2x41 = mat2x4(0.0);
    const mat2x4 cmat2x41 = mat2x4(0.0);

    dmat2x2 dmat2x21 = dmat2x2(0.0);
    const dmat2x2 cdmat2x21 = dmat2x2(0.0);
    dmat2x3 dmat2x31 = dmat2x3(0.0);
    const dmat2x3 cdmat2x31 = dmat2x3(0.0);
    dmat2x4 dmat2x41 = dmat2x4(0.0);
    const dmat2x4 cdmat2x41 = dmat2x4(0.0);


    mat3x2 mat3x21 = mat3x2(0.0);
    const mat3x2 cmat3x21 = mat3x2(0.0);
    mat3x3 mat3x31 = mat3x3(0.0);
    const mat3x3 cmat3x31 = mat3x3(0.0);
    mat3x4 mat3x41 = mat3x4(0.0);
    const mat3x4 cmat3x41 = mat3x4(0.0);

    dmat3x2 dmat3x21 = dmat3x2(0.0);
    const dmat3x2 cdmat3x21 = dmat3x2(0.0);
    dmat3x3 dmat3x31 = dmat3x3(0.0);
    const dmat3x3 cdmat3x31 = dmat3x3(0.0);
    dmat3x4 dmat3x41 = dmat3x4(0.0);
    const dmat3x4 cdmat3x41 = dmat3x4(0.0);

    
    mat4x2 mat4x21 = mat4x2(0.0);
    const mat4x2 cmat4x21 = mat4x2(0.0);
    mat4x3 mat4x31 = mat4x3(0.0);
    const mat4x3 cmat4x31 = mat4x3(0.0);
    mat4x4 mat4x41 = mat4x4(0.0);
    const mat4x4 cmat4x41 = mat4x4(0.0);

    dmat4x2 dmat4x21 = dmat4x2(0.0);
    const dmat4x2 cdmat4x21 = dmat4x2(0.0);
    dmat4x3 dmat4x31 = dmat4x3(0.0);
    const dmat4x3 cdmat4x31 = dmat4x3(0.0);
    dmat4x4 dmat4x41 = dmat4x4(0.0);
    const dmat4x4 cdmat4x41 = dmat4x4(0.0);
}


void main() {
    vec4 color = texture2D(colortex0, texcoord);
    vec3 normal = texture2D(colortex1, texcoord).xyz * 2.0 - 1.0;
    float depth = texture2D(gdepthtex, texcoord).r;

    functionTest();

    gl_FragData[0] = color;
}