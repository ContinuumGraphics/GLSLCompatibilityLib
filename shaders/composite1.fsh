#version 120
#include "/lib/extensions.glsl"

uniform sampler2D colortex0;
uniform sampler2D colortex1;

uniform sampler2D gdepthtex;
uniform mat4 gbufferProjectionInverse;

varying vec2 texcoord;

/* DRAWBUFFERS:0 */ 

void functionTest() {
    //Trig Functions
    float radianTest = radians(90.0);
    float degreeTest = degrees(radianTest);

    float sinTest = sin(degreeTest);
    float cosTest = cos(degreeTest);
    float tanTest = tan(degreeTest);

    float asinTest = asin(degreeTest);
    float acosTest = acos(degreeTest);
    float atanTest = atan(degreeTest, radianTest);

    float sinhTest = sinh(degreeTest);
    float coshTest = cosh(degreeTest);
    float tanhTest = tanh(degreeTest);

    //Exponentials
    float powerTest = pow(4.0, 5.0);
    float expTest = exp(5.0);
    float logTest = log(1.0);
    float exp2Test = exp2(1.0);
    float log2Test = log2(1.0);
    float sqrtTest = sqrt(5.0);
    float invSqrtTest = inversesqrt(5.0);

    //Common Functions
    float absTest = abs(-5.0);
    float signTest = sign(0.5);
    float floorTest = floor(0.5);
    float truncTest = trunc(-0.8);
    float roundTest = round(0.5);
    float roundEvenTest = roundEven(4.5);
    float ceilTest = ceil(0.5);
    float fractTest = fract(0.5);
    float modTest = mod(0.5, 0.5);
    int intOut;
    //float modfTest = modf(5.0, modfOut); Assuming this works, no idea though
    float minTest = min(1.0, 0.5);
    float maxTest = max(1.0, 5.0);
    float clampTest = clamp(1.0, 0.5, 2.0);
    float mixTest = mix(0.0, 1.0, 0.5);
    float stepTest = step(0.5, 1.0);
    float smoothstepTest = smoothstep(0.0, 1.0, 0.5);
    bool isnanTest = isnan(1.0);
    bool isinfTest = isinf(1.0);
    int floatBitsToIntTest = floatBitsToInt(5.2);
    uint floatBitsToUintTest = floatBitsToUint(5.2);
    float fmaTest = fma(1.0, 1.0, 5.0);
    float frexpTest = frexp(1.0, intOut);
    float ldexpTest = ldexp(0.5, 1);

    //pack Tests
    uint packTest = packSnorm4x8(vec4(1.0));
    vec4 unpackTest = unpackSnorm4x8(packTest);

    //Geometric Functions
    float lengthTest = length(vec3(0.65));
    float distanceTest = distance(0.0, 1.0);
    float dotTest = dot(vec4(1.0), vec4(1.0));
    vec3 crossTest = cross(vec3(0.0), vec3(1.0));
    vec3 normalizeTest = normalize(vec3(0.0));
    vec3 faceForwardTest = faceforward(vec3(1.0), vec3(1.0), vec3(1.0));
    vec3 reflectTest = reflect(vec3(1.0), vec3(1.0));
    vec3 refractTest = refract(vec3(1.0), vec3(1.0), 0.33);

    //Matrix Functions
    mat2 matCompMultTest = matrixCompMult(mat2(2.0), mat2(2.0));
    mat2 outerProductTest = outerProduct(vec2(0.0), vec2(0.0));
    mat2 transposeTest = transpose(mat2(1.0));
    float determinantTest = float(determinant(mat2(1.0f))); //Cast from double for some reason
    mat2 inverseTest = inverse(mat2(0.0));

    //vector relational
    bvec2 lessThanTest = lessThan(vec2(1.0), vec2(0.0));
    bvec2 lessThanEqualTest = lessThanEqual(vec2(1.0), vec2(0.0));
    bvec2 greaterThanTest = greaterThan(vec2(1.0), vec2(0.0));
    bvec2 greaterThanEqualTest = greaterThanEqual(vec2(1.0), vec2(0.0));
    bvec2 equalTest = equal(vec2(1.0), vec2(0.0));
    bvec2 notEqualTest = notEqual(vec2(1.0), vec2(0.0));
    bool anyTest = any(lessThanTest);
    bool allTest = all(lessThanTest);
    bvec2 notTest = not(lessThanTest);

    //Integer Functions
    uint outVal;
    uint uaddCarryTest = uaddCarry(1u, 1u, outVal);
    uint usubBorrowTest = usubBorrow(1u, 1u, outVal);
    umulExtended(1u, 1u, outVal, outVal); //Why is this void, stop glsl.
    uint bitfieldExtractTest = bitfieldExtract(1u, 1, 1);
    uint bitfieldInsertTest = bitfieldInsert(1u, 1u, 1, 1);
    uint bitfieldReverseTest = bitfieldReverse(1u);
    int  bitCountTest = bitCount(1);
    int  findLSBTest = findLSB(1);
    int findMSBTest = findMSB(1);

}


void main() {
    vec4 color = texture2D(colortex0, texcoord);
    vec3 normal = texture2D(colortex1, texcoord).xyz * 2.0 - 1.0;
    float depth = texture2D(gdepthtex, texcoord).r;

    functionTest();

    gl_FragData[0] = color;
}