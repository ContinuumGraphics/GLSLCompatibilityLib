#version 120

varying vec2 texcoord;
varying vec4 color;
varying vec3 normal;

uniform sampler2D texture;

vec4 albedo = color*texture2D(texture,texcoord);

void main() {
	//albedo.rgb=vec3(1.);
    gl_FragData[0] = albedo;
    gl_FragData[1] = vec4(normalize(normal) * 0.5 + 0.5, 1.0);
}