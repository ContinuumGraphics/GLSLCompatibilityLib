#version 400 compatibility

uniform sampler2D colortex0;
uniform sampler2D colortex1;

uniform sampler2D gdepthtex;
uniform mat4 gbufferProjectionInverse;

varying vec2 texcoord;

/* DRAWBUFFERS:0 */ 

void functionTest() {

}


int bitfieldReverset(int x) {
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1);
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2);
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4);
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8);
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16);
    return x;
}


void main() {
    vec4 color = texture2D(colortex0, texcoord);
    vec3 normal = texture2D(colortex1, texcoord).xyz * 2.0 - 1.0;
    float depth = texture2D(gdepthtex, texcoord).r;

    functionTest();
    float c;
    int x;
    for (int i = -100; i <= 100; i++) {
        int test = bitfieldReverset(i);
        int actual = bitfieldReverse(i);

        if (test == actual) {

        } else {
            c++;
        }

    }

    gl_FragData[0] = vec4((c / 201));
}